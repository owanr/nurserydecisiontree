import java.io.*;
import java.util.*;
import java.lang.*;
/*
Final Project for Data Mining
Decision Tree for Nursery Dataset
Written by Brynna Mering and Risako Owan
*/

public class testNDT {
	//Maps instance ID to data
	private static HashMap<Integer, String[]> entireSet = new HashMap<Integer, String[]>();
	private static HashMap<Integer, String> allLabels = new HashMap<Integer, String[]>();

	//private static HashMap<Integer, String[]> testingSet = new HashMap<Integer, String[]>();

	private static HashMap<Integer, String[]> possibleValues = new HashMap<Integer, String[]>();
		//Integer = feature index
		//String[] = possible values for that feature

    List<Integer> targetAtrributeIndex = 8;

    private static Integer numRows = 12960;

	/*
    ** Reads data from given fileName file
    ** Divides into training set and testing set (80/20)
    ** Divided using the shuffle and slice method
    */
   //DATA FORMAT
   // parents  3      usual, pretentious, great_pret
   // has_nurs  5     proper, less_proper, improper, critical, very_crit
   // form      4     complete, completed, incomplete, foster
   // children  4     1, 2, 3, more
   // housing   3     convenient, less_conv, critical
   // finance   2     convenient, inconv
   // social     3    non-prob, slightly_prob, problematic
   // health     2    recommended, priority, not_recom

	public static void fillsAllLabels(){
		allLabels.put(0,"parents");
		allLabels.put(1,"has_nurs");

	}

	/*
    ** Reads data from passed in fileName and fills entireSet
    */ 
    public static void readFile(String fileName) {
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            String[] splitLine;
           	Integer instanceID = 0;
            while (line != null && line.length() != 0) {
            	splitLine = line.split(",");
            	entireSet.put(instanceID,splitLine);
            	instanceID++;
	            line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Create random list of instanceIDs (for dividing into training and testing sets)
    */ 
    public static List<Integer> createRandomList(){
        List<Integer> randomList = new ArrayList<Integer>(entireSet.keySet());
        Collections.shuffle(randomList);
        return randomList;
    }

    public static HashMap<String,Double> calculateProbability(List<List<String>> allInstances, Integer featureIndex){
    	
        //Initializing and declaring count dictionary
        HashMap<String,Double> count = new HashMap<String,Double>();
        for (String possibleVal : possibleValues.get(featureIndex)){
        	count.put(possibleVal,0.0);
        }
        //Gets counts for each possible values of the feature at given featureIndex
        for (List<String> everyInstance : allInstances){
        	String category = everyInstance.get(featureIndex);
        	count.put(category,count.get(category)+1);
        }
        //Divides counts by n in order to get probability by count
        Integer tempNumInstance = allInstances.size();
        for (String possibleVal : count.keySet()){
			count.put(possibleVal,count.get(possibleVal)/tempNumInstance);
        }
        return count;
    }

    //Gets probabilities from calculate probabilities
    public static Double calculateEntropy(HashMap<String,Double> probabilities, Integer featureIndex){
        Double entropy = 0.0;
        //String[] possibleVals = possibleValues.get(featureIndex);
        for (String featureValue : probabilities.keySet()){
        	Double featureValueProb = probabilities.get(featureValue);
        	entropy -= featureValueProb*Math.log(featureValueProb)/Math.log(2)
        }
        return entropy;
    }

    //returns the id (how do we identify attributes) which has the lowest entropy
    //it takes the list of available attributes
    public static Integer findLowestEntropy(List<Integer> availableAttributes, List<Integer> availableExamples){
    	Integer lowestEntropy = 0;
    	return lowestEntropy;
    }

    public static void buildDecisionTree(List<Integer> currentAttributes, List<Integer> currentTrainingExamples){
        Integer attributeWithLowestEntropy = findLowestEntropy(currentAttributes, currentTrainingExamples);
        HashMap<Integer, HashMap> root = new HashMap<Integer, HashMap>();

        if(calculateEntropy(attributeWithLowestEntropy)==0){
            return root w/ lable attributeWithLowestEntropy 
        } else if(availableAttributes.size()==0){
            return root w/ lable of most common value of target in the set
        } else{

        }
    }

    public static HashMap<Integer,HashMap> recursiveHashTree(Integer[] frequentItem) {
        System.out.println("entered recursive with" + Arrays.toString(frequentItem));
        //base case
        if (frequentItem.length == 2) {
            HashMap<Integer,Integer> lastDict = new HashMap<Integer,Integer>();
            lastDict.put(frequentItem[1], 0);
            HashMap<Integer,HashMap> dict1 = new HashMap<Integer,HashMap>();
            dict1.put(frequentItem[0],lastDict);
            return dict1;
        //recursive case
        } else {
            HashMap<Integer,HashMap> dict2 = new HashMap<Integer,HashMap>();
            Integer[] newArray = makeSubarrayCopy(frequentItem);
            //Arrays.copyOfRange(frequentItem, 1, frequentItem.length);
            dict2.put(frequentItem[0], recursiveHashTree(newArray));
            return dict2;
        }
    }

	public static void main(String[] args){
		readFile("nursery.data");//WORKS
		List<Integer> randomizedIndexes = createRandomList();//WORKS
	}
}