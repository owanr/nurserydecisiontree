import java.io.*;
import java.util.*;
import java.lang.*;
/*
Final Project for Data Mining due 3/16
Decision Tree for Nursery Dataset
Written by Brynna Mering and Risako Owan
*/

public class NDT {
	// entireSet maps instance ID to data, which is a list of string
	private static List<List<String>> entireSet = new ArrayList<List<String>>();
	// allLabels maps attribute index to its name
	private static List<String> allLabels = new ArrayList<String>();
	// possibleValues maps the attribute index to its possible values
	private static HashMap<Integer, String[]> possibleValues = new HashMap<Integer, String[]>();
	// targetAttributeIndex is the index of the final classification answer
    private static Integer targetAttributeIndex = 8; //Use 4 when testing whether data
    // numRows is the number of instances we have
    private static Integer numRows = 12960;


	/*
	** Fills in allLabels, which maps attribute indexes to their attribute names.
	** allLabels is used in buildDecisionTree() and testDataPoint()
	*/
	public static void fillsAllLabels(){
		allLabels.add(0,"parents");
		allLabels.add(1,"has_nurs");
		allLabels.add(2,"form");
		allLabels.add(3,"children");
		allLabels.add(4,"housing");
		allLabels.add(5,"finance");
		allLabels.add(6,"social");
		allLabels.add(7,"health");
		allLabels.add(8,"accepted"); //the label we want to estimate
	}

	// // Used for weather data example testing
	// public static void fillsAllLabelsTest(){
	// 	allLabels.add(0,"outlook");
	// 	allLabels.add(1,"temperature");
	// 	allLabels.add(2,"humidity");
	// 	allLabels.add(3,"wind");
	// 	allLabels.add(4,"playTennis");
	// }

	// // Used for weather data example testing
	// public static void fillsPossibleValuesTest(){
	// 	String[] outlook = {"Sunny", "Overcast", "Rain"};
	// 	possibleValues.put(0, outlook);
	// 	String[] temperature = {"Hot", "Mild", "Cool"};
	// 	possibleValues.put(1, temperature);
	// 	String[] humidity = {"High", "Normal"};
	// 	possibleValues.put(2, humidity);
	// 	String[] wind = {"Weak", "Strong"};
	// 	possibleValues.put(3, wind);
	// 	String[] playTennis = {"No", "Yes"};
	// 	possibleValues.put(4, playTennis);
	// }

	/*
	** Fills in possibleValues, which maps all feature variables to their possible values.
	** possibleValues is used in buildDecisionTree() and calculateProbability()
	*/
	public static void fillsPossibleValues(){
		String[] parent = {"usual", "pretentious", "great_pret"};
		possibleValues.put(0, parent);
		String[] has_nurs = {"proper", "less_proper", "improper", "critical", "very_crit"};
		possibleValues.put(1, has_nurs);
		String[] form = {"complete", "completed", "incomplete", "foster"};
		possibleValues.put(2, form);
		String[] children = {"1", "2", "3", "more"};
		possibleValues.put(3, children);
		String[] housing = {"convenient", "less_conv", "critical"};
		possibleValues.put(4, housing);
		String[] finance = {"convenient", "inconv"};
		possibleValues.put(5, finance);
		String[] social = {"nonprob", "slightly_prob", "problematic"};
		possibleValues.put(6, social);
		String[] health = {"recommended", "priority", "not_recom"};
		possibleValues.put(7, health);
		String[] label = {"not_recom", "recommend", "very_recom", "priority", "spec_prior"};
		possibleValues.put(8, label);
	}

	/*
    ** Reads data from passed in fileName and fills entireSet, which is a list
    ** Then shuffles this list, using a seed
    */ 
    public static void readFile(String fileName) {
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            String[] splitLine;
            while (line != null && line.length() != 0) {
            	splitLine = line.split(",");
            	entireSet.add(Arrays.asList(splitLine));
	            line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
            //Shuffles dataset list
            Collections.shuffle(entireSet, new Random(123));
        }
    }

    /*
    ** Takes a portion of the dataset and index of current attribute
    ** Returns the probabilities of all possible values for that attribute
    */ 
    public static HashMap<String,Double> calculateProbability(List<List<String>> allInstances, Integer featureIndex){
        //Initializing and declaring count dictionary
        HashMap<String,Double> count = new HashMap<String,Double>();
        for (String possibleVal : possibleValues.get(featureIndex)){
        	count.put(possibleVal,0.0);
        }
        //Gets counts for each possible values of the feature at given featureIndex
        for (List<String> everyInstance : allInstances){
        	String category = everyInstance.get(featureIndex);
        	count.put(category,count.get(category)+1);
        }
        //Divides counts by n in order to get probability by count
        Integer tempNumInstance = allInstances.size();
        
        for (String possibleVal : count.keySet()){
			count.put(possibleVal,count.get(possibleVal)/tempNumInstance);
        }
        return count;

    }

    /*
    ** Calculates entropy for given dataset portion, using probabilities from calculateProbability()
    */
    public static Double calculateEntropy(List<List<String>> allInstances){
    	HashMap<String,Double> probabilities = calculateProbability(allInstances, targetAttributeIndex);
        Double entropy = 0.0;
        for (String featureValue : probabilities.keySet()){
        	Double featureValueProb = probabilities.get(featureValue);
        	Double logValue;
        	if (featureValueProb == 0.0){
        		logValue = 0.0;
        	} else {
        		logValue = Math.log(featureValueProb)/Math.log(2);
        	}

        	entropy -= featureValueProb*logValue;
        }
        return entropy;
    }

    /*
    ** Returns all data instances with the given value in specified attribute.
    */
    public static List<List<String>> getSubSet(List<List<String>> availableExamples, Integer attribute, String value){
    	List<List<String>> subSet = new ArrayList<List<String>>();
        for(List<String> example : availableExamples){
            if(example.get(attribute).equals(value)){
                subSet.add(example);
            }
        }
        return subSet;
    }

    /*
    ** Returns the information gain of the given dataset collection and attribute index
    ** Used in findHighestInformationGain method
    */
    public static Double findInformationGain(List<List<String>> availableExamples, Integer currentAttribute){
    	Double entropy = calculateEntropy(availableExamples);
    	// probabilities maps a value to its probability
        HashMap<String,Double> probabilities = calculateProbability(availableExamples, currentAttribute);
        Double valueSum = 0.0;
        // probabilities.keySet() = the set of all the possible values in given index
        for(String value : probabilities.keySet()){
            List<List<String>> subSet = getSubSet(availableExamples, currentAttribute, value);
            if (!subSet.isEmpty()){
            	valueSum += (probabilities.get(value))*calculateEntropy(subSet);
            }
        }
        return (entropy - valueSum);
    }

    /*
    ** Returns the highest information gain attribute of the given dataset collection and attribute list
    */
    public static Integer findHighestInformationGain(List<List<String>> availableExamples, List<Integer> availableAttributes){
        Double highestGain = 0.0;
        Integer split = null;
        for(Integer attribute : availableAttributes){
            Double valueSum = findInformationGain(availableExamples, attribute);
            if(valueSum >= highestGain){
                highestGain = valueSum;
                split = attribute;
            }
        }
        return split;
    }

    /*
    ** Returns the most common class in the given dataset
    */
    public static String getMostCommonClass(List<List<String>> allInstances){
    	//Initializing and declaring class dictionary
        HashMap<String,Integer> classCount = new HashMap<String,Integer>();

        classCount.put("not_recom",0);
        classCount.put("recommend",0);
        classCount.put("very_recom",0);
        classCount.put("priority",0);
        classCount.put("spec_prior",0);     

        //WEATHER DATA
        // classCount.put("No",0);
        // classCount.put("Yes",0);    

        //Gets counts for each possible values of the feature at given featureIndex
        for (List<String> everyInstance : allInstances){
        	String classVal = everyInstance.get(targetAttributeIndex);
        	classCount.put(classVal,classCount.get(classVal)+1);
        }
        
        //Gets most common
        String mostCommon = "";
        Integer mostCommonNum = 0;
        for (String possibleClass : classCount.keySet()){
        	if (classCount.get(possibleClass) >= mostCommonNum){
        		mostCommon = possibleClass;
        		mostCommonNum = classCount.get(possibleClass);
        	}
        }
        return mostCommon;
    }

    /*
    ** Returns index of the given className
    */
    public static Integer getMostCommonClassIndex(String className){
    	//NURSERY
    	if (className.equals("not_recom")){
    		return 0;
    	} else if (className.equals("recommend")){
    		return 1;
    	} else if (className.equals("very_recom")){
    		return 2;
    	} else if (className.equals("priority")){
    		return 3;
    	} else { //"spec_prior"
    		return 4;
    	}

    	//WEATHER
    	// if (className.equals("No")){
     // 		return 0;
     // 	} else {
     // 		return 1;
     // 	}
    }

    public static HashMap<List<String>, HashMap> buildDecisionTree(List<List<String>> currentTrainingExamples, List<Integer> currentAttributes){
        //Creating a root node for the tree
        HashMap<List<String>, HashMap> root = new HashMap<List<String>, HashMap>();

    	// BASE CASE
    	// If everything is in one category or if we can split on no more attributes
        if ((calculateEntropy(currentTrainingExamples) == 0) || (currentAttributes.size()==0)){
        	Integer classIndex = getMostCommonClassIndex(getMostCommonClass(currentTrainingExamples));
        	List<String> label = new ArrayList<String>();
        	label.add(allLabels.get(targetAttributeIndex));
        	label.add(possibleValues.get(targetAttributeIndex)[classIndex]);
        	root.put(label,null);
        	return root;
        }

        //OTHERWISE BEGIN 
        Integer newRootAttribute = findHighestInformationGain(currentTrainingExamples,currentAttributes);
        String[] newRootAttributeValues = possibleValues.get(newRootAttribute);

        //For each possible value of the new root attribute...

        for (Integer valIndex = 0; valIndex < newRootAttributeValues.length; valIndex++){
        	List<List<String>> subSet = getSubSet(currentTrainingExamples, newRootAttribute, newRootAttributeValues[valIndex]);
        	if (subSet.isEmpty()){
	        	Integer classIndex = getMostCommonClassIndex(getMostCommonClass(currentTrainingExamples));
	        	List<String> label = new ArrayList<String>();
	        	label.add(allLabels.get(targetAttributeIndex));
	        	label.add(possibleValues.get(targetAttributeIndex)[classIndex]);
	        	root.put(label,null);
        	} else {
	    		List<Integer> updatedAttributes = new ArrayList<Integer>(currentAttributes);
	        	updatedAttributes.remove(newRootAttribute);
	        	HashMap<List<String>, HashMap> newTree = buildDecisionTree(subSet, currentAttributes);
	        	List<String> label = new ArrayList<String>();
	        	label.add(allLabels.get(newRootAttribute));
	        	label.add(newRootAttributeValues[valIndex]);
	        	root.put(label, newTree);
        	}
        }
        return root;
    }

    @SuppressWarnings("unchecked")
    public static boolean testDataPoint(HashMap<List<String>, HashMap> tree, List<String> dataPoint){
        List<List<String>> root = new ArrayList<List<String>>(tree.keySet());
        String rootAttribute = root.get(0).get(0);
        int index = allLabels.indexOf(rootAttribute);
        for(List<String> node : root){
        	//Node is [class, value] while data point is the entire array of strings
            if(tree.get(node) == null){
            	List<String> keyVal = new ArrayList<String>();
            	keyVal.add(rootAttribute);
            	keyVal.add(dataPoint.get(index));
                if(node.equals(keyVal)){
                    return true;
                } else{
                    return false;
                }
            } else{
                if((node.get(1)).equals(dataPoint.get(index))){
                    return testDataPoint(tree.get(node), dataPoint);
                }
             }
        }
        return false;
    }

    public static Double testTreeWithGivenPercent(Double percentNum){
        Double amountTrue = 0.0;
        List<Integer> attributes = new ArrayList<Integer>();
        List<List<String>> testSet = new ArrayList<List<String>>();
        List<List<String>> treeSet = new ArrayList<List<String>>();
        for (int j = 0; j < targetAttributeIndex; j ++){
                attributes.add(j);
        }
        //Assuming entireSet.size() is divisible by 10
        //Nursery data size is 12960
    	Integer denom =((Double)(entireSet.size()*percentNum)).intValue();
        for(int i = 0; i<entireSet.size(); i+=denom){
        	//Puts 10% in testSet and remaining 90% in treeSet.
	            testSet = entireSet.subList(i, i+denom);
	            treeSet.clear();
	            treeSet.addAll(entireSet);
	            treeSet.subList(i, (i+denom)).clear();    

            HashMap<List<String>, HashMap> tree = new HashMap<List<String>, HashMap>(buildDecisionTree(treeSet, attributes));
            for(List<String> testPoint : testSet){
                boolean accurate = testDataPoint(tree, testPoint);
                if(accurate == true){
                    amountTrue += 1.0;
                }
            }
        }
        return amountTrue/entireSet.size();
    }

    //Use BFS to traverse down tree
    @SuppressWarnings("unchecked")
    public static void printTopNSubtreesOfTree(HashMap<List<String>, HashMap> decisionTreeRoot, Integer subtreeNum){
    	Queue<HashMap> hashMapQueue = new LinkedList<HashMap>();
    	hashMapQueue.add(decisionTreeRoot);
    	HashMap<List<String>, HashMap> currentHashMap;
    	Integer subtree = 1;
    	while (subtree <= subtreeNum && !hashMapQueue.isEmpty()){
			currentHashMap = hashMapQueue.poll();
			if (currentHashMap != null){
				for (List<String> key : currentHashMap.keySet()){
					HashMap<List<String>, HashMap> newHashMap = currentHashMap.get(key);
					if (newHashMap != null)
						System.out.print("From "+key + ": " + newHashMap.keySet() + "\n\n");
						hashMapQueue.add(newHashMap);
				}
			}
			subtree++;
    	}
    }

	public static void main(String[] args){
		readFile("nursery.data");
		fillsAllLabels();
		fillsPossibleValues();
		// readFile("weather.data");
		// fillsPossibleValuesTest();
		// fillsAllLabelsTest();

		List<Integer> attributes = new ArrayList<Integer>();
		for (Integer i = 0; i < targetAttributeIndex; i ++){
			attributes.add(i);
		}

		HashMap<List<String>, HashMap> decisionTree = buildDecisionTree(entireSet,attributes);


		System.out.println("5% gets "+testTreeWithGivenPercent(0.05));
		System.out.println("10% gets "+testTreeWithGivenPercent(0.10));
		System.out.println("20% gets "+testTreeWithGivenPercent(0.20));
		System.out.println("25% gets "+testTreeWithGivenPercent(0.25));
		System.out.println("50% gets "+testTreeWithGivenPercent(0.50));
		System.out.println();
		System.out.println("Please uncomment code in main to print decision tree.");
		System.out.println("Warning: The tree is very large.");
		//System.out.println(decisionTree);
		//System.out.println("-----------");
		//printTopNSubtreesOfTree(decisionTree, 10);
	}
}